using System.Collections.Generic;
using UnityEngine;

public sealed class Platform : MonoBehaviour
{
    [SerializeField]
    private List<Transform> points;

    [SerializeField]
    private PlatformMovingType movingType;

    private IPlatformMovingStrategy platformMovingStrategy;

    [Space]
    [SerializeField]
    private float speed = 5.0f;

    private Transform currentPoint;

    private void Start()
    {
        this.currentPoint = points[0];
        this.transform.position = points[0].position;

        platformMovingStrategy = GetMovingStrategy();
    }

    private IPlatformMovingStrategy GetMovingStrategy()
    {
        switch (movingType)
        {
            case PlatformMovingType.Cycle:
                return new PlatformCycleMovingStrategy(points);

            case PlatformMovingType.Path:
                return new PlatformPathMovingStrategy(points);
        }

        Debug.LogError("Moving strategy is undefined");
        return null;
    }

    private void FixedUpdate()
    {
        this.Move();
    }

    private void Move()
    {
        var myPosition = this.transform.position;
        var targetPosition = this.currentPoint.position;

        var distanceVector = targetPosition - myPosition;
        var distance = distanceVector.magnitude;
        this.TryChangePoint(distance);

        var direction = distanceVector.normalized;
        var velocity = direction * (this.speed * Time.fixedDeltaTime);
        this.transform.position += velocity;
    }


    private void TryChangePoint(float distance)
    {
        if (distance <= 0.1f)
        {
            currentPoint = platformMovingStrategy.GetNext();
        }
    }
}
