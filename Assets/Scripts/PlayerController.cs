using System.Linq;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public Collider2D _collider2D;

    private Rigidbody2D _rigidbody2D;
    private Animator _animator;
    private SpriteRenderer _spriteRenderer;

    public LayerMask ground;
    public LayerMask wall;

    [SerializeField] float wallJumpX;
    [SerializeField] float wallJumpY;

    void Start()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _animator = GetComponentInChildren<Animator>();
        _spriteRenderer = GetComponentInChildren<SpriteRenderer>();
    }

    void Update()
    {
        float hDirection = Input.GetAxis("Horizontal");

        if (hDirection < 0.0f)
        {
            _rigidbody2D.velocity = new Vector2(-5f, _rigidbody2D.velocity.y);
            _spriteRenderer.flipX = true;
            _animator.SetBool("isRunning", true);
        }
        else if (hDirection > 0.0f)
        {
            _rigidbody2D.velocity = new Vector2(5f, _rigidbody2D.velocity.y);
            _spriteRenderer.flipX = false;
            _animator.SetBool("isRunning", true);
        }
        else
        {
            _rigidbody2D.velocity = new Vector2(0f, _rigidbody2D.velocity.y);
            _animator.SetBool("isRunning", false);
        }

        if (Input.GetKeyDown(KeyCode.Space) && _collider2D.IsTouchingLayers(ground))
        {
            Debug.Log("JUMP");
            _rigidbody2D.velocity = new Vector2(_rigidbody2D.velocity.x, 5f);
        }
        else if (Input.GetKeyDown(KeyCode.Space) && isLeftWallTouch)
        {
            Debug.Log("LEFT WALL JUMP");
            _rigidbody2D.velocity = Vector3.zero;
            _rigidbody2D.angularVelocity = 0;
            _rigidbody2D.AddForce(new Vector2(wallJumpX, wallJumpY));
        }
        else if (Input.GetKeyDown(KeyCode.Space) && isRightWallTouch)
        {
            Debug.Log("RIGHT WALL JUMP");
            _rigidbody2D.velocity = Vector3.zero;
            _rigidbody2D.angularVelocity = 0;
            _rigidbody2D.AddForce(new Vector2(-wallJumpX, wallJumpY));
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Bonus"))
        {
            Destroy(other.gameObject);
        }
    }

    public bool isLeftWallTouch = false;
    public bool isRightWallTouch = false;

    public void OnCollisionEnter2D(Collision2D collision)
    {
        isLeftWallTouch = collision.contacts.Count(x => x.normal.x == 1) > 0;
        isRightWallTouch = collision.contacts.Count(x => x.normal.x == -1) > 0;

        if (isLeftWallTouch)
            Debug.Log("LEFT WALL TOUCH");

        if (isRightWallTouch)
            Debug.Log("RIGHT WALL TOUCH");
    }

    public void OnCollisionExit2D(Collision2D collision)
    {
        Debug.Log("OnCollisionExit2D");

        isLeftWallTouch = collision.contacts.Count(x => x.normal.x == 1) != 0;
        isRightWallTouch = collision.contacts.Count(x => x.normal.x == -1) != 0;

        if (!isLeftWallTouch)
            Debug.Log("LEFT WALL NO TOUCH");

        if (!isRightWallTouch)
            Debug.Log("RIGHT WALL NO TOUCH");
    }
}
