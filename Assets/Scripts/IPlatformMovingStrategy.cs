﻿using UnityEngine;

public interface IPlatformMovingStrategy
{
    Transform GetNext();
}
