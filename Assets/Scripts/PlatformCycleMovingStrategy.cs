﻿using System.Collections.Generic;
using UnityEngine;

public class PlatformCycleMovingStrategy : IPlatformMovingStrategy
{
    private List<Transform> _points;
    private int currentPointIndex = 0;

    public PlatformCycleMovingStrategy(List<Transform> points)
    {
        _points = points;
    }

    public Transform GetNext()
    {
        currentPointIndex = (currentPointIndex + 1) % _points.Count;
        return _points[currentPointIndex];
    }
}