﻿using System.Collections.Generic;
using UnityEngine;

public class PlatformPathMovingStrategy : IPlatformMovingStrategy
{
    private List<Transform> _points;

    private int currentPointIndex = 0;
    private int indexDir = 1;

    public PlatformPathMovingStrategy(List<Transform> points)
    {
        _points = points;
    }

    public Transform GetNext()
    {
        int nextIdx = currentPointIndex + indexDir;

        if (nextIdx < 0 || nextIdx == _points.Count)
            indexDir *= -1;

        currentPointIndex += indexDir;
        return _points[currentPointIndex];
    }
}
